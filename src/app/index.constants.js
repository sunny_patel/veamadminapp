/* global malarkey:false, moment:false */
(function() {
  'use strict';

  angular
    .module('veamAdminApp')
    .constant('malarkey', malarkey)
    .constant('moment', moment);

})();
