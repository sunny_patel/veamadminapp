(function() {
  'use strict';

  angular
    .module('veamAdminApp')
    .config(routerConfig);

  /** @ngInject */
  function routerConfig($stateProvider, $urlRouterProvider) {
    $stateProvider
      .state('main', {
        url: '/',
        templateUrl: 'app/main/main.html',
        controller: 'MainController',
        controllerAs: 'vm'
      })
      .state('main.createRestaurant', {
        url: 'createRestaurant',
        templateUrl: 'app/components/restaurants/createRestaurant.html',
        controller: 'RestaurantsController',
        controllerAs: 'vm'
      })
      .state('main.newFood', {
        url: 'newFood',
        templateUrl: 'app/components/food/newFood.html',
        controller: 'FoodController',
        controllerAs: 'vm'
      });

    $urlRouterProvider.otherwise('createRestaurant');
  }

})();
