(function() {
  'use strict';

  angular
    .module('veamAdminApp')
    .factory('restaurantsService', restaurantsService);

    restaurantsService.$inject = [];

    function restaurantsService () {
      
      return {
        new: function() {
          return {
            name: "",
            address: "",
            address2: "",
            city: "",
            state: "",
            zipcode: "",
            phone: "",
            contact: {
              managersName: "",
              managersPhone: ""
            }
          }
        }

      }

    }


})();
