(function() {
  'use strict';

  angular
    .module('veamAdminApp')
    .run(runBlock);

  /** @ngInject */
  function runBlock($log) {

    $log.debug('runBlock end');
  }

})();
