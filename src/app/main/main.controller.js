(function() {
  'use strict';

  angular
    .module('veamAdminApp')
    .controller('MainController', MainController);

  /** @ngInject */
  function MainController($scope, $mdSidenav) {
    var vm = this;
    vm.openLeftMenu = function() {
      $mdSidenav('left').toggle();
    };
  }
})();
