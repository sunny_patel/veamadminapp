(function() {
  'use strict';

  angular
    .module('veamAdminApp')
    .controller('RestaurantsController', restaurantsController);

  /** @ngInject */
  function restaurantsController($mdSidenav, restaurantsService) {
    var vm = this;
    vm.newResta = restaurantsService.new();

    vm.openLeftMenu = function() {
      $mdSidenav('left').toggle();
    };

    vm.createRestaurant = function () {
      console.log(vm.newResta);
    }
  }
})();
