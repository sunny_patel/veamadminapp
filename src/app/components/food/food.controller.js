(function() {
  'use strict';

  angular
    .module('veamAdminApp')
    .controller('FoodController', foodController);

  /** @ngInject */
  function foodController($scope, $mdSidenav) {
    var vm = this;
    vm.openLeftMenu = function() {
      $mdSidenav('left').toggle();
    };
  }
})();
