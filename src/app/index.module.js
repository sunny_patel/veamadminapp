(function() {
  'use strict';

  angular
    .module('veamAdminApp', ['ngMaterial', 'ngAnimate', 'ngCookies',  'ngSanitize', 'ngMessages', 'ngAria', 'ngResource', 'ui.router', 'toastr']);

})();
